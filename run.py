import json
from time import sleep
from typing import List, Generator

import pandas as pd
import requests

api_url = "https://blockchain.info/rawaddr"


def extract_addresses(fname: str) -> List[str]:
    """ Get list of crypto addresses from a file """
    with open(fname) as jfile:
        address_list = json.load(jfile)
        addresses = [d["cryptocurrency_address"] for d in address_list]
    if not isinstance(addresses, list):
        raise TypeError(
            f"Variable addresses should be a list not type "
            f"{type(addresses).__name__}"
        )
    return addresses


def get_transactions(addresses: List[str]) -> Generator:
    """ Get list of transaction IDs for a list of crypto addresses """
    for address in addresses:
        while True:
            response = requests.get(url=f"{api_url}/{address}")
            if response.status_code != 200:
                try:
                    print(
                        f"Error {response.status_code}: response {response.json()}"
                    )
                except json.decoder.JSONDecodeError:
                    print(f"Error HTTP {response.status_code}. Sleeping for 5 mins")
                    sleep(305)
            else:
                for tx in response.json()["txs"]:
                    yield tx["hash"]
                break


def dump_tx_ids(txs: Generator) -> None:
    with open("external_tx_ids.json", "w") as output_file:
        json.dump(list(txs), output_file)


def open_as_list(fname: str) -> List[str]:
    """ Open a file as a list of strings """
    df = pd.read_csv(fname)
    #return df[(df.transaction_type == "ASSETS_EXTERNAL")]["external_transaction_id"]
    return df["external_transaction_id"]


if __name__ == "__main__":
    #dump_tx_ids(
    #    txs=get_transactions(
    #        addresses=extract_addresses(fname="crypto_addresses.json")
    #    )
    #)
    with open("external_tx_ids.json") as jfile:
        blockchain_txs = set(json.load(jfile))
    db_txs = set(open_as_list(fname="swp_sec_txs.csv"))
    print(f"blockchain_txs: {blockchain_txs} with length: {len(blockchain_txs)}")
    print(f"db_txs: {db_txs} with length: {len(db_txs)}")
    print(
        f"difference: blockchain {len(blockchain_txs)} and db {len(db_txs)} "
        f"= {len(blockchain_txs) - len(db_txs)}"
    )
    diff = blockchain_txs - db_txs
    print(f"missing tx ids: {diff} with length: {len(diff)}")
    with open("missing_tx_ids.txt", "w") as outfile:
        for tx_id in diff:
            outfile.write(f"{tx_id}\n")
